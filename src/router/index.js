// import Vue from 'vue'
// import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import store from '../store'

// Vue.use(VueRouter)


// 判断环境是否是微应用打开
let microPath = ''
if (window.__POWERED_BY_QIANKUN__) {
  microPath = '/vue2-micro-app'
}

const routes = [
  {
    path: microPath + '/',
    redirect: microPath + '/home'
  },
  {
    name: 'Home',
    path: microPath + '/home',
    component: Home
  },
  {
    name: 'About',
    path: microPath + '/about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    name: 'login',
    path: microPath + '/login',
    component: () => import(/* webpackChunkName: "about" */ '../views/login.vue')
  }
]

// const router = new VueRouter({
//   routes
// })

// // 路由守卫
// router.beforeEach((to, from, next) => {
//   if (to.path !== (microPath + '/login')) {
//     if (store.state.token) {
//       console.log("已经登录 token=",store.state.token)
//       if (window.__POWERED_BY_QIANKUN__ && !to.path.includes('vue2-micro-app')) {
//         next(microPath + to.path)
//       } else {
//         next()
//       }
//     } else {
//       console.log("子应用 - 未登录 请登录")
//       next(microPath + '/login')
//     }
//   } else {
//     next()
//   }
// })

export default routes
